import { Component, OnInit } from '@angular/core';
import { HeaderFooterService } from '../service/header-footer.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{

  home: boolean = false;

  constructor(private headerService : HeaderFooterService) { 

  }
  ngOnInit(): void {
   this.headerService.changementPage(this.home);
  }

}
