import { EventEmitter, Injectable, Output } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HeaderFooterService {

  @Output()
  change: EventEmitter<boolean> = new EventEmitter();

  changementPage(home: boolean){
  this.change.emit(home);
}


  constructor() { }
}
