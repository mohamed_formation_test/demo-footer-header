import { Component } from '@angular/core';
import { HeaderFooterService } from '../service/header-footer.service';

@Component({
  selector: 'app-page2',
  templateUrl: './page2.component.html',
  styleUrls: ['./page2.component.css']
})
export class Page2Component {

  home: boolean = true;

  constructor(private headerService : HeaderFooterService) { 

  }
  ngOnInit(): void {
   this.headerService.changementPage(this.home);
  }

}
