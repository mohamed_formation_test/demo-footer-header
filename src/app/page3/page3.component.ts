import { Component } from '@angular/core';
import { HeaderFooterService } from '../service/header-footer.service';

@Component({
  selector: 'app-page3',
  templateUrl: './page3.component.html',
  styleUrls: ['./page3.component.css']
})
export class Page3Component {
  home: boolean = true;

  constructor(private headerService : HeaderFooterService) { 

  }
  ngOnInit(): void {
   this.headerService.changementPage(this.home);
  }
}
