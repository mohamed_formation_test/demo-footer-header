import { Component } from '@angular/core';
import { HeaderFooterService } from 'src/app/service/header-footer.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {

  home: boolean = false;

  constructor(private headerFooterService: HeaderFooterService) { }

  ngOnInit(): void {
    this.headerFooterService.change.subscribe(home => {

      this.home = home;

    })
  }

}
