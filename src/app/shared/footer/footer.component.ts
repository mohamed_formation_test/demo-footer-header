import { Component } from '@angular/core';
import { HeaderFooterService } from 'src/app/service/header-footer.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent {

  home: boolean = false;

  constructor(private headerFooterService: HeaderFooterService) { }

  ngOnInit(): void {
    this.headerFooterService.change.subscribe(home => {

      this.home = home;

    })
  }
}
